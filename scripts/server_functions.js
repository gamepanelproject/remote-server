var config 		= require('../config');

var fs 			= require('fs');
var execa 		= require('execa');

var childProc	= require('child_process');
var spawn 		= require('child_process').spawn;
var exec 		= require('child_process').exec;

var pusage 		= require('pidusage');
var Q 			= require('q');

var getServiceStatus = function(service) {
	var globals		= require('./globals');
	var async 		= require('async');
	var os			= require('os');
	
	var deferred = Q.defer();
	
	var installDir 	= globals.userDir+'/'+service.OwnerID+'/'+service.IP+"_"+service.PORT;
	var updatePID 	= globals.workingDir+'/pids/updates/'+service.ServiceID+".pid";
	var servicePID	= globals.workingDir+'/pids/services/'+service.ServiceID+".pid";
	
	async.parallel([function(callback) {
		fs.stat(installDir, function(err, stats) {
			if(err) {
				callback(null, false);
			} else {
				callback(null, true);
			}
		});
	},
	function(callback) {
		fs.readFile(updatePID, function(err, data) {
			
			if(err) {
				callback(null, false);
			} else {
				data = (data.toString().trim());
				pusage.stat(data, function(err, stat) {
					if(err) {
						callback(null, false);
							
					} else {
						callback(null, stat);
					}
				});
			};
		});
	},
	function(callback) {
		fs.readFile(servicePID, function(err, data) {
			
			if(err) {
				callback(null, false);
			} else {
				data = (data.toString().trim());
				pusage.stat(data, function(err, stat) {
					if(err) {
						callback(null, false);
							
					} else {
						callback(null, stat);
					}
				});
			};
		});
	}],
	function(err, results) {
		console.log(results);
		
		var isInstalled = results[0];
		var isUpdating 	= results[1];
		var isRunning 	= results[2];
		
		var resultData = {};
		
		resultData.curStatus 	= 0;
		resultData.cpu			= 0;
		resultData.mem			= {};
		resultData.mem.bytes	= 0;
		resultData.mem.perc		= 0;
		
		if(!isInstalled) {
			resultData.curStatus = 4;
		} else if(!isRunning && isUpdating) {
			resultData.curStatus = 2;
		} else if(!isRunning && !isUpdating && isInstalled) {
			resultData.curStatus = 1;
		} else if(isRunning) {
			resultData.curStatus = 3;
			
			resultData.cpu			= results[2].cpu;
			resultData.mem			= {};
			resultData.mem.bytes	= results[2].memory;
			resultData.mem.perc		= (results[2].memory/globals.totalMemory);
		}
		
		deferred.resolve(resultData);
	});
	
	return deferred.promise;
}

var installService = function(service, template) {
	updateService(service, template);
};

var updateService = function(service, template) {
	var globals		= require('./globals');
	
	var pidFile 	= globals.workingDir+'/pids/updates/'+service.ServiceID+".pid";
	var logFile 	= globals.workingDir+'/logs/updates/'+service.ServiceID+".log";
	
	var installDir 	= globals.userDir+'/'+service.OwnerID+'/'+service.IP+"_"+service.PORT;
	
	var steamCMD 	= spawn(globals.toolsDir+'/steamcmd/steamcmd.'+globals.exeType, [ 
		'+login anonymous', 
		'+force_install_dir '+installDir, 
		'+app_update '+template.SteamAppID+' validate',
		'+quit'
	]);
	
	fs.writeFile(pidFile, steamCMD.pid, 'utf8');
	
	var wstream = fs.createWriteStream(logFile);
	
	steamCMD.stdout.setEncoding('utf-8');
	steamCMD.stdout.on('data', function(data) {
		wstream.write(data);
	});
	
	globals.updatePipe[service.ServiceID] = steamCMD.stdout;
	
	steamCMD.on('error', function() {
		fs.unlink(pidFile);
	});
	
	steamCMD.on('exit', function(code, signal) {
		fs.unlink(pidFile);
	});
};

var startService = function(service, template) {
	//var master		= require('./master_connection');
	var globals		= require('./globals');
	var os			= require('os');
	
	var pidFile 	= globals.workingDir+'\\pids\\services\\'+service.ServiceID+".pid";
	var installDir 	= globals.userDir+'\\'+service.OwnerID+'\\'+service.IP+"_"+service.PORT;
	
	var deferred 	= Q.defer();
	
	var template = template; 
	fs.access(installDir, fs.F_OK, function(err) {
		if (err) {
			//Send error message to client
			deferred.resolve(false);
			return;
		}
		
		var exe;
		if(os.platform() == "win32") {
			exe = template.WindowsExe;
		} else if(os.platform() == "linux") {
			exe = template.LinuxExe;
		} else {
			deferred.resolve(false);
			return;
		}
		
		var binary	= installDir+template.WorkingDir+"\\"+exe;
		
		var cmdLine = {};
		
		var serviceCMDsF = {};

		var serviceCMDs = JSON.parse(template.CmdLine);
		var customCMDs = JSON.parse(service.ServiceCustomCMD);
			
		serviceCMDs.forEach(function(cmdOption) {
			cmdLine[cmdOption[0]] = cmdOption[1];
			serviceCMDsF[cmdOption[0]] = [cmdOption[1], cmdOption[2], cmdOption[3]];
		})
			
		customCMDs.forEach(function(cmdOption) {
			if(cmdLine[cmdOption[0]]) {
				if(serviceCMDsF[cmdOption[0]][1]) {
					cmdLine[cmdOption[0]] = cmdOption[1];
				}
			} else {
				cmdLine[cmdOption[0]] = cmdOption[1];
			}
		})
			
		console.log(cmdLine);
			
		for(var cmdOption in cmdLine) {
			exe += (" " + cmdOption + " " + cmdLine[cmdOption]);
		}
			
		// Debug 
		exe = exe.replace('-ip', '');
		exe = exe.replace('[IP]', '');
		//
			
		//exe = exe.replace('[IP]', service.ServiceIP);
		exe = exe.replace('[Port]', service.PORT);
		exe = exe.replace('[Slots]', service.MaxSlots);
		
		//if(service.GameProcessType == 1) {
		//	exe += (" " + service.GameProcessIDArg + " " + pidFile);
		//}

		var server = childProc.exec(exe, {cwd:installDir+template.WorkingDir+"/"});
		fs.writeFile(pidFile, server.pid, 'utf8');
		
		//globals.updatePipe[service.ServiceID] = steamCMD.stdout;
		
		deferred.resolve(true);
	});
	
	return deferred.promise;
};

var stopService = function(service, template) {
	var deferred 	= Q.defer();
	var globals		= require('./globals');
	var pidFile 	= globals.workingDir+'/pids/services/'+service.ServiceID+".pid";
	
	getServiceStatus(service, template).then(function(result) {
		if(result.curStatus == 3) {
			console.log(result);
			fs.readFile(pidFile, function(err, data) {
				if(err) {
					deferred.resolve(false);
				} else {
					data = (data.toString().trim());
					process.kill(data);
					deferred.resolve(true);
				};
			});
		} else {
			deferred.resolve(false);
		}
	});
	
	return deferred.promise;
}

module.exports = {
	getServiceStatus: getServiceStatus,
	stopService: stopService,
	startService: startService,
	installService: installService
}