var config	= require('../config');
var os		= require('os');
var fs		= require('fs');
var Q 		= require('q');
var nPath	= require('path');

var masterAPI = require('./master.api');

var rootDir			= "C:\\gsp-users";
module.exports = {
	localServices	: [],
	templates		: {},
	
	userId			: null,
	
	getRootNameList: function() {
		var names = [];
		
		for(var i=0; i<module.exports.localServices.length; i++) {
			names[i] = module.exports.localServices[i].NickName;
		}
		
		return names;
	},
	
	getServiceIndexByNickName: function(nickName) {
		for(var i=0; i<module.exports.localServices.length; i++) {
			if(module.exports.localServices[i].NickName == nickName) {
				return i;
			}
		}
		return -1;
	},
	
	validatePath: function(path, callback) {
		var resolvedPath 	= nPath.resolve(path);
		var arr	 			= path.split("\\");
		var index 			= module.exports.getServiceIndexByNickName(arr[1]);
		
		if(index > -1) {
			// List Virtual Folder
			var service = module.exports.localServices[index];
			
			arr.shift(); // Remove slash
			arr.shift(); // Remove service name 
			
			var FTPDir = module.exports.templates[service.TemplateID].FTPDir;
			
			console.log(rootDir+"\\"+service.OwnerID+"\\"+service.IP+"_"+service.PORT+"\\"+FTPDir+"\\"+arr.join("\\"));
			
			resolvedPath = nPath.resolve(rootDir+"\\"+service.OwnerID+"\\"+service.IP+"_"+service.PORT+"\\"+FTPDir+"\\"+arr.join("\\"));
			
			callback(null, resolvedPath);
		} else {
			callback(true, resolvedPath);
		}
	},

	validatePathSync: function(path) {
		var resolvedPath 	= nPath.resolve(path);
		var arr	 			= path.split("\\");
		var index 			= module.exports.getServiceIndexByNickName(arr[1]);
		
		if(index > -1) {
			// List Virtual Folder
			var service = module.exports.localServices[index];
			
			arr.shift(); // Remove slash
			arr.shift(); // Remove service name
			
			var FTPDir = module.exports.templates[service.TemplateID].FTPDir;
			
			console.log(rootDir+"\\"+service.OwnerID+"\\"+service.IP+"_"+service.PORT+"\\"+FTPDir+"\\"+arr.join("\\"));
			
			resolvedPath = nPath.resolve(rootDir+"\\"+service.OwnerID+"\\"+service.IP+"_"+service.PORT+"\\"+FTPDir+"\\"+arr.join("\\"));
			
			return resolvedPath;
		} else {
			return false;
		}
	},

	setup: function(userID, callback) {
		//Get local services setup dir pathing for root
		module.exports.userId = userID;
		
		masterAPI.getUserAuthorizedLocalServices(userID).then(function(data) {
			module.exports.localServices = data.services;
			module.exports.templates	 = data.templates;
			
			callback();
		}, function(error) {
			callback(error);
		});
	},
	
	unlink: function(path, callback) {
		console.log('[VFS] unlink');
		
		module.exports.validatePath(path, function(hasEscaped, resolvedPath) {
			fs.unlink(resolvedPath, function(err) {
				callback(err);
			});
		})
	},
	
	readdir: function(path, options, callback) {
		console.log('[VFS] readdir');
		
		var args = [];
		for (var i = 0; i < arguments.length; i++) {
			args.push(arguments[i]);
		}
		path 		= args.shift();
		callback 	= args.pop();
		
		if (args.length > 0) options = args.shift(); else options = null;

		if(path == "\\") {
			callback(null, module.exports.getRootNameList());
			return;
		}
		
		module.exports.validatePath(path, function(hasEscaped, resolvedPath) {
			if(!hasEscaped) {
				fs.readdir(resolvedPath, options, function(err, dir) {
					callback(err, dir);
				});
			} else {
				callback(true, null);
			}
		})
	},
	
	mkdir: function(path, mode, callback) {
		console.log('[VFS] mkdir');
		
		var args = [];
		for (var i = 0; i < arguments.length; i++) {
			args.push(arguments[i]);
		}
		path 		= args.shift();
		callback 	= args.pop();
		
		if (args.length > 0) mode = args.shift(); else mode = null;
		
		module.exports.validatePath(path, function(hasEscaped, resolvedPath) {
			fs.mkdir(resolvedPath, mode, function(err) {
				callback(err);
			});
		})
	},
	
	open: function(path, flags, mode, callback) {
		console.log('[VFS] open');
		
		var args = [];
		for (var i = 0; i < arguments.length; i++) {
			args.push(arguments[i]);
		}
		path 		= args.shift();
		flags 		= args.shift();
		callback 	= args.pop();

		if (args.length > 0) mode = args.shift(); else mode = null;
		
		module.exports.validatePath(path, function(hasEscaped, resolvedPath) {
			fs.open(resolvedPath, flags, mode, function(err, fd) {
				callback(err, fd);
			});
		})
	},
	
	close: function(fd, callback) {
		console.log('[VFS] close');
		fs.close(fd, function(err) {
			callback(err);
		});
	},
	
	rmdir: function(path, callback) {
		console.log('[VFS] rmdir');
		
		module.exports.validatePath(path, function(hasEscaped, resolvedPath) {
			fs.rmdir(resolvedPath, function(err) {
				callback(err);
			});
		})
	},
	
	rename: function(oldPath, newPath, callback) {
		console.log('[VFS] rename');
		
		var resolvedPath = module.exports.validatePathSync(oldPath);
		if(!resolvedPath) {
			callback(true, null);
			return;
		}
		
		var resolvedPathNew = module.exports.validatePathSync(newPath);
		if(!resolvedPathNew) {
			callback(true, null);
			return;
		}
		
		fs.rename(resolvedPath, resolvedPathNew, function(err) {
			callback(err);
		})
	},
	
	stat: function(path, callback) {
		console.log('[VFS] stat');
	
		var arr	 			= path.split("\\");
		if(arr.length == 2) {
			var stats 			= {};
			stats.mode 			= 0;
			stats.isDirectory 	= function() {return true};
			stats.size 			= 0;
			stats.mtime 		= '2017-07-01T00:05:52.104Z';
			callback(null, stats);
		
			return;
		}

		module.exports.validatePath(path, function(hasEscaped, resolvedPath) {
			fs.stat(resolvedPath, function(err, stats) {
				callback(err, stats);
			})
		})
	},
	
	createWriteStream: function(path, options) {
		console.log('[VFS] createWriteStream');
		
		if(!options) {
			options = null;
		}
		
		var resolvedPath = module.exports.validatePath(path);
		if(resolvedPath) {
			return fs.createWriteStream(resolvedPath, options);
		} else {
			return false;
		}
	},
	
	writeFile: function(file, data, options, callback) {
		console.log('[VFS] writeFile');
		
		if(!options) {
			options = null;
		}
	},
	
	createReadStream: function(path, options) {
		console.log('[VFS] createReadStream');
		
		if(!options) {
			options = null;
		}
		
		if(path != null) {
			var resolvedPath = module.exports.validatePathSync(path);
			if(resolvedPath) {
				return fs.createWriteStream(resolvedPath, options);
			} else {
				return false;
			}
		} else {
			return fs.createReadStream(path, options);
		}
	},
	
	readFile: function(path, options, callback) {
		console.log('[VFS] readFile');
		
		if(!options) {
			options = null;
		}
	}
};