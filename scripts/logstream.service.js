var config 	= require('../config');

var express = require('express');
var helmet 	= require('helmet');
var async 	= require('async');
var http 	= require('http');

var globals = require('./globals');
var master 	= require('./master.api');
var fs		= require('fs');

var path = require('path');
var redis   = require('redis');

var app = express();

app.set('etag', false); // turn off
app.use(helmet());

app.use(function(req, res, next) {
	console.log("LOG VIEWER");
	res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
	res.header('Access-Control-Allow-Origin', '*');
	next();
});

var streamFile = function(file, res, id) {
	console.log("TRYING TO STREAM " + file)
	var fileT 	= file;
	var resT	= res;
	var idT		= id;
	fs.stat(file, function(err, stat) {
		if(err == null) {
			var readableStream 	= fs.createReadStream(file);
			readableStream.setEncoding('utf8');
			readableStream.on('data', function(chunk) {
				//console.log(chunk);
				res.write("id: " + Date.now() + "\ndata: " + JSON.stringify(chunk) + "\n\n");
			});
			
			readableStream.on('end', function () {
				console.log('end');
				
				try {
					globals.updatePipe[id].on('data', function(data) {
						res.write("id: " + Date.now() + "\ndata: " + JSON.stringify(data) + "\n\n");
					});
				} catch(err) {};
			});
			
		} else {
			setTimeout(function() {
				streamFile(fileT, resT, idT);
			}, 1500)
		}
	});
}

app.get('/update/:ServiceID/stream/:AuthToken', master.validateLogAPIToken, function(req, res, next) {
	res.writeHead(200, {
		'Content-Type': 'text/event-stream',
		'Cache-Control': 'no-cache',
		'Connection': 'keep-alive'
    })
	
	console.log(req.params.AuthToken);
	
	var logFile 		= globals.workingDir+'/logs/updates/'+req.params.ServiceID+".log";

	streamFile(logFile, res, req.params.ServiceID);
})


var streamConsoleLog = function(file, res) {
	console.log("TRYING TO STREAM " + file)
	var fileT 	= file;
	var resT	= res;
	fs.stat(file, function(err, stat) {
		if(err == null) {
			var currentLength = 0;
			
			var readableStream 	= fs.createReadStream(file);
			readableStream.setEncoding('utf8');
			readableStream.on('data', function(chunk) {
				currentLength += chunk.length;
				res.write("id: " + Date.now() + "\ndata: " + JSON.stringify(chunk) + "\n\n");
			});
			
			readableStream.on('end', function () {
				console.log('end');
				
				try {
					
					var logFileWatcher = fs.watchFile(file, {interval:1000}, function(event, filename) {
						console.log("EVENT VOIEWIOIWJNDJOKWandwaiojawd[io")
						console.log(event);
						console.log(currentLength);
						var changedBytes = event.size-currentLength;
						console.log(changedBytes);
						
						
						fs.open(file, 'r', function(err, fd) {
							if(!err) {
								var buffer = Buffer.alloc(changedBytes);
								fs.read(fd, buffer, 0, changedBytes, currentLength, function(err, bytesRead, buffer) {
									res.write("id: " + Date.now() + "\ndata: " + JSON.stringify(buffer.toString('utf-8',0,changedBytes)) + "\n\n");
									currentLength += changedBytes;
								})
							}
						})
					});
				} catch(err) {
					console.log(err);
				};
			});
		} else {
			setTimeout(function() {
				streamFile(fileT, resT);
			}, 1500)
		}
	});
}

app.get('/console/:ServiceID/stream/:AuthToken', master.validateLogAPIToken, master.getServiceLog, function(req, res, next) {
	res.writeHead(200, {
		'Content-Type': 'text/event-stream',
		'Cache-Control': 'no-cache',
		'Connection': 'keep-alive'
    })
	
	var logFile 		= req.logFile;

	streamConsoleLog(logFile, res);
})

var httpServer = http.createServer(app).listen(3076);