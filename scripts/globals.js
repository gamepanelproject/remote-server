var config	= require('../config');
var os		= require('os');

var Globals 		= {};
Globals.updatePipe 	= {};
Globals.consolePipe = {};

if(os.platform() == "linux") {
	Globals.userDir 	= config.linux_dir;
	Globals.workingDir 	= config.working_dir_linux;
	Globals.exeType		= "sh";
	Globals.cmdType		= "bash";
}

if(os.platform() == "win32") {
	Globals.userDir 	= config.windows_dir;
	Globals.workingDir 	= config.working_dir_windows;
	Globals.exeType		= "exe";
	Globals.cmdType		= "cmd";
}

Globals.totalMemory = os.totalmem();

Globals.toolsDir = Globals.workingDir+"/tools"

module.exports = Globals;