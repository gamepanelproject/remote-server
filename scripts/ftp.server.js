var config		= require('../config');
//var master	= require('./master_connection');

var ftpd 		= require('ftpd');
var util 		= require('util');
var masterAPI	= require('./master.api.js');
var options = {
	getInitialCwd: function(connection) {
		return "/";
    },
	
	getRoot: function(connection) {
		return "/";
    }
};
 
var host = '0.0.0.0';
var server = new ftpd.FtpServer(host, options);
 
server.on('client:connected', function(conn) {
	var username;
	
	console.log('Client connected from ' + conn.socket.remoteAddress);
	
	conn.on('command:user', function(user, success, failure) {
		username = user;
		success();
	});
	
	conn.on('command:pass', function(pass, success, failure) {
		// check the password
		
		masterAPI.checkFTPLogin(username, pass).then(function(result) {
			if(result) {
				var vfs = require('./vfs');
				vfs.setup(1, function(error) {
					if(error) {
						failure();
						return;
					}
					success(username, vfs);
				})
			} else {
				failure();
			}
		})
	});
});
 
server.listen(21);
console.log('FTPD listening on port 21');