var config		= require('../config');
var io 			= require('socket.io-client');
var Q 			= require('q');
var fs 			= require('fs');

// Connect to server
var socket = io.connect(config.master_ip, {transports: ['websocket'], upgrade: false});

socket.authed = false;

// Add a connect listener
socket.on('connect', function(socket) { 
	console.log('Connecting...');
});

require('./master.api/auth')(socket);
require('./master.api/control')(socket);
require('./master.api/status')(socket);


module.exports = socket;