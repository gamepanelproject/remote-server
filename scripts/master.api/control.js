var config 		= require('../../config');
var serverFuncs	= require('../server_functions');

module.exports = function(socket) {
	socket.on("remote/service/update", function(data, callback) {
		console.log('remote/service/update');
		serverFuncs.installService(data.service, data.template);
		callback(true);
	});

	socket.on("remote/service/start", function(data, callback) {
		console.log('remote/service/start');
		serverFuncs.startService(data.service, data.template).then(function(result) {
			callback(result);
		});
	});

	socket.on("remote/service/stop", function(data, callback) {
		console.log('remote/service/stop');
		serverFuncs.stopService(data.service, data.template).then(function(result) {
			callback(result);
		})
	});
	
	socket.on("remote/service/restart", function(data, callback) {
		console.log('remote/service/restart');
		callback(true);
		/*
		serverFuncs.stopService(data.serviceID).then(function(result) {
			console.log(result);
			callback(result);
		});
		*/
	});
	
	socket.on("remote/service/reinstall", function(data, callback) {
		console.log('remote/service/reinstall');
		callback(true);
		/*
		serverFuncs.stopService(data.serviceID).then(function(result) {
			console.log(result);
			callback(result);
		});
		*/
	});
}