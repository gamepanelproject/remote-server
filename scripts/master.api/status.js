var config 		= require('../../config');
var serverFuncs	= require('../server_functions');
var osUtils  	= require('os-utils');
var os  		= require('os');
var disk 		= require('diskspace');
var async 		= require('async');

var path = os.platform() === 'win32' ? 'c' : '/';

module.exports = function(socket) {
	socket.on("remote/service/status", function(data, callback) {
		console.log('remote/service/status');
		
		serverFuncs.getServiceStatus(data.service).then(function(result) {
			callback(result);
		});
	});
	
	socket.on("remote/stats", function(data, callback) {
		console.log('remote/stats');
		
		async.parallel([function(cb) {
			osUtils.cpuUsage(function(v) {
				var returnData 		= {};
				
				returnData.status	= 1;
				returnData.cpuUsage = v;
				returnData.platform = osUtils.platform();
				returnData.cpus 	= osUtils.cpuCount();
				returnData.upTime	= osUtils.sysUptime();
				returnData.bitness	= os.arch();
				returnData.totalMem	= os.totalmem()
				returnData.freeMem	= os.freemem();
				
				cb(null, returnData);
			});
		},
		function(cb) {
			disk.check(path, function (err, total, free, status) {
				var returnData 		= {};
				
				if(err) {
					returnData.status	= 0;
					cb(err, returnData)
					return;
				}
					
				returnData.hddTotal = total;
				returnData.hddFree	= free;
					
				cb(null, returnData);
			});
		}],
		function(err, results) {
			callback(results);
		});
	});
}