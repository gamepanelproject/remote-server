var config 		= require('../../config');
var Q 			= require('q');
var serverFuncs	= require('../server_functions');

module.exports = function(socket) {
	socket.on('auth/remote/request', function (data) {
		console.log('auth/remote/request...');
		socket.emit('auth/remote/response', { sid: config.server_id, key: config.master_auth });
	});

	socket.on('auth/remote/accepted', function (data) {
		console.log('Remote Server Authenticated');
		socket.authed = true;
	});

	socket.on('auth/remote/rejected', function (data) {
		console.log('Remote Server Authenticated');
	});
	
	socket.checkFTPLogin = function(email, pass) {
		console.log("util/ftpLogin");
		var deferred = Q.defer();
		
		socket.emit('util/ftpLogin', {
			email: email, 
			pass: pass
		}, function(result) {
			deferred.resolve(result);
		});
		
		return deferred.promise;
	}
	
	socket.getUserAuthorizedLocalServices = function(userID) {
		console.log("util/getLocalServices");
		var deferred = Q.defer();
		
		socket.emit('util/getLocalServices', {
			user: userID, 
			server: config.server_id
		}, function(result) {
			console.log(result);
			deferred.resolve(result);
		});
		
		return deferred.promise;
	}
	
	socket.validateToken = function(token) {
		console.log("auth/client/token");
		var deferred = Q.defer();
		
		socket.emit('auth/client/token', {
			token: token
		}, function(data) {
			deferred.resolve(data);
		})
		
		return deferred.promise;
	}
}