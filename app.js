var config	= require('./config');
var os		= require('os');
var fs 		= require('fs');

//===============MAIN===============
// Setup
var osDir = false;
if(os.platform() == "linux") {
	osDir = config.linux_dir;
}
if(os.platform() == "win32") {
	osDir = config.windows_dir;
}

if(osDir) {
	if (!fs.existsSync(osDir)) {
		fs.mkdirSync(osDir);
	}
} else {
	console.log("!!!!! PLATFORM NOT SUPPORTED - EXITING !!!!!");
	process.exit(1);
}

require('./scripts/ftp.server');
//require('./scripts/client.api');
require('./scripts/master.api');
//require('./scripts/logstream.service');