// config.js
module.exports = {
	//Application Details
	"server_id"		: 1,
	"host_port"		: 13337,
	"master_auth"	: "remoteServerPassword",
	"master_ip"		: "http://localhost",
	"master_port"	: 80,
	
	"working_dir_linux": "/home/gsp-remote",
	"working_dir_windows": "c:\\gsp-remote",
	"linux_dir"		: "/home/gsp-users",
	"windows_dir"	: "c:\\gsp-users"
}